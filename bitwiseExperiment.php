<?php
$messages = [
    "0","1","2","3","4","5","6","7"
];

$charString="010080000000";

$bytesArray = str_split($charString,2);
$resultMessages = [];
$j = 0;
foreach ($bytesArray as $byte) {
    $raisedBytes = str_pad(decbin(hexdec($byte)),8,0,STR_PAD_LEFT);
    echo "Raised Bytes: ".$raisedBytes.PHP_EOL;
    for ($i=0;$i < strlen($raisedBytes);$i++) {
        if($raisedBytes[$i] === "1") {
            $resultMessages[] = $messages[strlen($raisedBytes) - 1 - $i];
        }
    }
    echo "Byte :".$j." messages: ".PHP_EOL;
    echo json_encode($resultMessages).PHP_EOL;
    echo "endMessages for Byte ".$j.PHP_EOL;
    $j++;
    $resultMessages = [];
}

