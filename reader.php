<?php
$TIMEOUT = 10;
$fd = dio_open( "/dev/ttyUSB0", O_RDWR | O_NONBLOCK );
dio_tcsetattr( $fd, array(
    "baud"         => 115200,
    "bits"         => 8,
    "stop"         => 1,
    "parity"       => 0,
    "flow_control" => 0,
    "is_canonical" => 0	));
//dio_write( $fd, ’temperatur?’ );
$time_start = time();
$input = "";
while($time_start + $TIMEOUT > time())
{
    $input .= dio_read( $fd );
    usleep( 100000 );
}
echo "Received: ".$input.PHP_EOL;
dio_close( $fd );
?>