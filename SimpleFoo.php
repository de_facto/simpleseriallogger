<?php
require_once "Foo.php";

class SimpleFoo implements Foo
{
    private function __construct() {

    }
    public static function factory(): Foo
    {
        return new SimpleFoo();
    }

    public function identity(): string
    {
        return "SimpleFoo";
    }
}