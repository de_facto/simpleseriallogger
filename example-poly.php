<?php
require_once "SimpleFoo.php";
require_once "FooImpl.php";


$availableFoos = [
    SimpleFoo::class,
    FooImpl::class
];

foreach ($availableFoos as $fooClass) {
    $foo = $fooClass::factory();
    echo $foo->identity().PHP_EOL;
}