<?php

require_once "Foo.php";
class FooImpl implements Foo
{
    private function __construct() {

    }
    public static function factory(): Foo
    {
        return new FooImpl();
    }

    public function identity(): string
    {
        return "FooImpl";
    }
}