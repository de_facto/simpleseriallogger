<?php


interface Foo
{
    public static function factory() : Foo;
    public function identity() : string;
}