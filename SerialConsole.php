<?php

require_once "SerialLogger.php";
class SerialConsole extends SerialLogger
{
    private $stdin;
    private const MAX_READ_RETRIES = 5;

    public function run(): void
    {
        //$this->setBaudRate();
        //$this->openSerialSocket();
        //$this->openFileSocket();
        $this->openStdin();
        $this->promptUser();
        $this->openSerialSocket();
        while (trim($buffer = fgets($this->stdin,1024)) !== "exit") {
            if(strlen($buffer) > 0) {
                $this->prepareAndWriteCommand(trim($buffer));
                //usleep(5000);
                $read = $this->readFromSerial();
                fputs(STDOUT,"<<< ".$read."[".strlen($read)."]".PHP_EOL);
                $this->promptUser();
            }
        }
        $this->closeSerialSocket();
    }
    public function trialRun() {
        $this->openSerialSocket();
        $name = "Прасе";
        $name2 = "Гошо        01";
        //$command = "729844IS017298-0001-19789750000200000000999000004501200".iconv("UTF-8","CP1251",$name);
        $command = "729875".iconv("utf-8","cp1251",$name2);
        $this->prepareAndWriteCommand($command);
        fputs(STDOUT,"<<< ".$this->readFromSerial().PHP_EOL);
    }
    private function openStdin() {
        $this->stdin = fopen("php://stdin",'r');
    }
    private function promptUser() {
        fputs(STDOUT, "####!!! Write a command to serial and press Enter... !!!####\nSerialConsole> ");
    }
    private function prepareAndWriteCommand(string $command) : void {
        $this->writeToSerial($this->prepareCommandString(trim($command)));
    }

    private function prepareCommandString(string $command) : string {
        $Lenght=strlen($command)+6;
        $Lenght=dechex($Lenght);
        if(strlen($Lenght)<2)
        {
            $LB1="0x3"."0";
            $LB2="0x3".$Lenght;
        }
        else
        {
            $LB1="0x3".$Lenght[0];
            $LB2="0x3".$Lenght[1];
        }
        $LB1=hexdec($LB1);
        $LB2=hexdec($LB2);
        $Str_LB1=chr($LB1);
        $Str_LB2=chr($LB2);
        $command.=$Str_LB1;
        $command.=$Str_LB2;
        $Sum=0;
        $Output_command=chr(2);
        for ($i = 0; $i < strlen($command); $i++)
        {
            $Output_command.=$command[$i];
            $Sum_part=str_pad(ord($command[$i]), 2, '0', STR_PAD_LEFT);
            $Sum += $Sum_part;
        }
        $Sum+=2;
        $Sum&=255;
        $Sum=dechex($Sum);
        $Sum_str=$Sum;
        if(strlen($Sum_str) <2)
        {
            $CRCP2_t="0x3".$Sum_str;
            $CRCP1_t="0";
        }
        else
        {
            $CRCP1_t="0x3".$Sum_str[0];
            $CRCP2_t="0x3".$Sum_str[1];
            $CRCP1_t=chr(hexdec($CRCP1_t));
        }
        $CRCP2_t=chr(hexdec($CRCP2_t));
        $Output_command.=$CRCP1_t;
        $Output_command.=$CRCP2_t;
        $Output_command.=chr(3);

        return $Output_command;
    }
    private function writeToSerial(string $command) : void {
        //$this->setBaudRate();

        //$command = iconv("utf-8","cp1251",$command);

        $written = dio_write($this->serialSocket,$command,strlen($command));
        flush();
        fputs(STDOUT, "Written Bytes: ".$written.PHP_EOL);
        fputs(STDOUT, ">>> ".$command.PHP_EOL);
    }
    private function readFromSerial() : string {
        //return dio_read($this->serialSocket) ?? "Can't read from device";
        //fflush($this->serialSocket);
        for($i = 0; $i < self::MAX_READ_RETRIES; $i++) {
            //flush();
            $read = dio_read($this->serialSocket,4096);
            fputs(STDOUT, "Read is: ".$read."[".mb_strlen($read)."]".PHP_EOL);
            if($read == "\x15" ) {
                return "NACK";
            }
            if($read == "\x06") {
                return "ACK";
            }
            if($read == "\x05") {
                fputs(STDOUT, "<<< WAIT [1]".PHP_EOL);
                $i = 0;
                continue;
            }
            if(mb_strlen($read) < 1) {

                //fflush($this->serialSocket);
                //flush();
                //fputs(STDOUT, "Got : ".$read." With length : ".strlen($read).PHP_EOL);
                //fputs(STDOUT, "Retrying...".PHP_EOL);
                usleep(110000);
                continue;
            }

            return $read;
        }
        /*fputs(STDOUT,"DIO STAT: ".json_encode(dio_stat($this->serialSocket)).PHP_EOL);*/
        fputs(STDOUT, "WARNING !!! : Couldn't read from device...\n");
        return "";
    }

    private function closeSerialSocket() : void {
        //flush();
        dio_close($this->serialSocket);
    }
}