<?php

class SerialLogger 
{
    private  $handle;
    private  $baudRate;
    private  $outputFile;

    protected $serialSocket;
    private $fileSocket;

    public function __construct(
         string $handle = "/dev/ttyS0",
         int $baudRate=115200,
         $outputFile = "serial"
        )
    {
        $this->handle = $handle;
        $this->baudRate = $baudRate;
        $this->outputFile = date("Y-m-d-H-i-s")."-".$outputFile.".log";
    }
    public function __destruct()
    {
        //dio_close($this->serialSocket);
        //fclose($this->fileSocket);
    }
    public function run() : void {
        $this->openSerialSocket();
        $this->setBaudRate();
        //$this->openFileSocket();
        $timer = 100000;
        while(true) {
            if(($buffer = fread($this->serialSocket,1024)) !== false) {
                $buffer = $this->prependTime($buffer);
                $this->writeBuffer($buffer);
                $timer = 300000;
            }
            $timer--;
            if($timer <= 0 ) {
                echo "Timeout reached \n";
                break;
            }
        }
        
    }
    protected function prependTime($buffer) : string {
        return "[".date("Y-m-d H:i:s")."]".trim($buffer)."\n";
    }
    protected function writeBuffer($buffer) : void {
        fputs($this->fileSocket,$buffer);
    }
    protected function openSerialSocket() : void {
        //$this->setBaudRate();
        $this->serialSocket = $this->openSocket($this->handle);
        //dio_fcntl($this->serialSocket, F_SETFL, O_ASYNC);
        dio_tcsetattr($this->serialSocket, array(
            'baud' => $this->baudRate,
            'bits' => 8,
            'stop'  => 1,
            'parity' => 0,
            'flow_control' => 0,
            'is_canonical' => 0
        ));
    }
    protected function openFileSocket() : void {
        //$this->fileSocket = $this->openSocket($this->outputFile);
    }
    protected function setBaudRate() : void {
        $command = "stty -F ".$this->handle." ".$this->baudRate."; stty -F ".$this->handle." -parity;stty -F ".$this->handle." -cooked";
        shell_exec($command);
    }
    protected function openSocket($handle) {
        return dio_open($handle,O_RDWR | O_NOCTTY | O_NONBLOCK);
    }
}