<?php

$allEncodings = mb_list_encodings();
$inputFile = "/home/alex/logs/testSerial.log";

foreach ($allEncodings as $srcEncoding) {
    foreach($allEncodings as $dstEncoding) {
        $fp = fopen($inputFile,"r");
        if($fp !== false ) {
            echo "*** SRCENCODING ".$srcEncoding." ***\n";
            echo "*** DSTENCODING ".$dstEncoding." ***\n";
            $outputFile = $srcEncoding."-to-".$dstEncoding.".log";
            $output = fopen($outputFile,"wrb+");
            while(!feof($fp)) {
                $line = fgets($fp);
                fputs($output,iconv($srcEncoding,$dstEncoding,$line));                
            }
            fclose($output);
            fclose($fp);
        }
    }
}